import React, { Component } from 'react'
import { registerRootComponent } from 'expo';
import Routes from './app/routes/Routes'
import { Root, StyleProvider } from 'native-base';
import { Loading } from './app/src/components';
import getTheme from './native-base-theme/components';
import material from './native-base-theme/variables/platform';
import { StatusBar } from 'expo-status-bar';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import moment from 'moment';
import 'moment/locale/id'
moment.locale('id');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };    
  }

  async componentDidMount() {
    await SplashScreen.preventAutoHideAsync();
    await Font.loadAsync({
      FiraSans: require('./assets/fonts/FiraSans-Regular.ttf'),
      FiraSans_medium: require('./assets/fonts/FiraSans-Medium.ttf'),
      FiraSans_bold: require('./assets/fonts/FiraSans-Bold.ttf'),
      FiraSans_black: require('./assets/fonts/FiraSans-Black.ttf'),
      FiraSans_extrabold: require('./assets/fonts/FiraSans-ExtraBold.ttf'),
      FiraSans_italic: require('./assets/fonts/FiraSans-Italic.ttf'),
      FiraSans_light: require('./assets/fonts/FiraSans-Light.ttf'),
      FiraSans_thin: require('./assets/fonts/FiraSans-Thin.ttf'),
    });
    return this.setState({ isReady: true }, async () => await SplashScreen.hideAsync() );
  }

  render() {
    if (!this.state.isReady) {
      return <Loading />;
    }
    return (
      <>
        <StatusBar backgroundColor="#fff" style="dark" />
        <StyleProvider style={getTheme(material)}>
          <Root>
            <Routes />
          </Root>
        </StyleProvider>
      </>
    )
  }
}



export default registerRootComponent(App);