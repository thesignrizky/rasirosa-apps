import { ENV } from './index';
import axios from 'axios';

export const POST = async (URL, payload, sucessCB, failedCB) => {
  return axios.post(`${ENV.HTTP_URL}${URL}`, payload, {
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Content-Type": "application/json",
    },
  }).then((res) => {
    if(res.status === 200){
      return sucessCB && sucessCB(res)
    }else{
      const err = { status: 400 }
      return failedCB && failedCB(err)
    }
  }).catch((err) => { 
    return failedCB && failedCB(err);
  })
}

export const GET = async (URL, sucessCB, failedCB) => {
  return axios.get(`${ENV.HTTP_URL}${URL}`).then((res) => {
    if(res.status === 200){
      return sucessCB && sucessCB(res)
    }else{
      const err = { status: 400 }
      return failedCB && failedCB(err)
    }
  }).catch((err) => {
    return failedCB && failedCB(err);
  })
}