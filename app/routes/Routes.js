import React, { Component } from 'react'
import * as Containers from '../src/containers';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
const Stack = createStackNavigator();

export default class Routes extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerStyle: { backgroundColor: '#fff'}, headerTintColor: '#00A2EA', }}>
          <Stack.Screen name="Home" component={Containers.Home} options={{ headerShown: false }} />
          <Stack.Screen name="Post" component={Containers.Post} options={{ title: 'Detail berita',
          headerBackTitleVisible: false
         }}/>
          <Stack.Screen name="Category" component={Containers.Category} options={{ title: '', headerBackTitleVisible: false }}/>
          <Stack.Screen name="Page" component={Containers.Page} options={{ title: '', headerBackTitleVisible: false }}/>
          <Stack.Screen name="Related" component={Containers.Related} options={{ title: 'Info Lainnya', headerBackTitleVisible: false }}/>
          <Stack.Screen name="Search" component={Containers.Search} options={{ headerShown: false }}/>
        </Stack.Navigator>
      </NavigationContainer>
    )
  } 
}
