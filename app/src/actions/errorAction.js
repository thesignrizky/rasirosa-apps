
import { message } from 'antd';

export const errorHandler = (err, cb) => async dispatch => {
  // console.log(222, err);
  switch (err.shortCode) {
    case "Error":
    case "UNAUTHORIZED":
      return null      
    case "SESSION_EXPIRED":
    case "INVALID_TOKEN":
    default:
      return cb
  }
  
}
