import { API} from '../../config';

export const setDetailPage = ( slug ) => {
  return new Promise((resolve, reject) => {
    return API.GET(`/pages?slug=${slug}`, (res) => {
      return setMenuNavbar((menu) => {
        return resolve({
          data: res.data[0],
          menu: menu
        })
      })
    }, (err) => {
      return reject(err);
    })

  })
}


export const setMenuNavbar = (successCB)  => {
  return API.GET('/menu', (res) => {
    return successCB(res.data);
  }, () => {
    var data = []
    return successCB(data);
  })
}
