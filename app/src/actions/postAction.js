import { API } from '../../config';
var qs = require('qs');

export const setListPost = (value)  => {
  return new Promise((resolve, reject) => {
    var query = value ? qs.stringify(value, { encode: false }) : null;
    return API.GET(`/posts?${query}`, (res)  => {
      return setMenuNavbar((menu) => {
        return resolve({
          data: res.data,
          menu: menu
        });
      })
    }, () => {
      var data = []
      return resolve(data);
    })
  })
}

export const setDetailPost = ( slug ) => {
  return new Promise((resolve, reject) => {
    return API.GET(`/posts?slug=${slug}`, (res) => {
      return setMenuNavbar((menu) => {
        return resolve({
          data: res.data[0],
          menu: menu
        })
      })
    }, (err) => {
      return resolve(err);
    })
  })
}

export const setHomePost = ()  => {
  return new Promise((resolve, reject) => {
    return API.GET('/home', (res) => {
      return setMenuNavbar((menu) => {
        var _res = { 
          post: res.data, 
          menu: menu
        }
        return resolve(_res);
      })
    }, () => {
      var data = []
      return resolve(data);
    })
  })
}

export const setHomeVideo = (successCB)  => {
  return API.GET('/posts?per_page=40&page=1&filter[category_name]=video', (res) => {
    return successCB(res.data);
  }, () => {
    var data = []
    return successCB(data);
  })
}

export const setSliderVideo = (successCB)  => {
  return API.GET('/slider', (res) => {
    return successCB(res.data);
  }, () => {
    var data = []
    return successCB(data);
  })
}

export const setMenuNavbar = (successCB)  => {
  return API.GET('/menu', (res) => {
    return successCB(res.data);
  }, () => {
    var data = []
    return successCB(data);
  })
}
