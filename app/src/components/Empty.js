import React, { Component } from 'react'
import { View, Image } from 'react-native';
import { Container, Content, H3, Text, Header, List, ListItem, Item, Icon, Input, Left, Body, Right, Title, Button,} from 'native-base';
import { global } from '../styles';

export default class Empty extends Component {
  render() {
    const { text } = this.props;
    return (
      <Container style={[{backgroundColor: '#fff'}]}>
        <Content contentContainerStyle={{flex: 1}} padder>
          <View style={[global.centered, {marginTop: -100}]}>
            <Icon name="inbox" type="AntDesign" style={{fontSize: 100, color: '#a2a2a2'}}/>
            <H3 style={{textTransform: 'capitalize', color: '#a2a2a2'}}>{text ? text : 'Data Kosong'}</H3>
          </View>
        </Content>
      </Container>
    )
  }
}
