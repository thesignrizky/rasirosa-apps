import React, { Component } from 'react'
import { View } from 'react-native'
import { List, ListItem, Thumbnail, Text, Left, Body, H3, Icon } from 'native-base'
import { global, html } from '../styles';
import moment from 'moment';
import HTMLView from 'react-native-htmlview';
import { Empty } from './';

export default class ListPost extends Component {
  render() {
    const { data, title } = this.props;
    const { navigation } = this.props.passProps;
    return (
      <View style={{marginVertical: 20}}>
        {title &&
          <View style={[global.l_container, {flexDirection: 'row',  marginBottom: 10, height: 25}]}>
            <View style={{width: 5, backgroundColor: '#FD1713', marginRight: 10}}/>
            <H3 bold style={[{fontSize: 22,  alignSelf: 'center'}]}>
              {title}
            </H3>
          </View>
        }
        <List style={{backgroundColor: '#fff'}}>
          {data.length > 0 ? data.map((res, i) => (
            <ListItem thumbnail key={i} button onPress={() => navigation.push('Post', {data: {post_name: res.slug}})}>
              <Left>
                {res.fimg_url.type === 'photo' ?
                  <Thumbnail source={res.fimg_url.file ? {uri: res.fimg_url.file} : require('../img/img-default.jpg')} square large style={{borderRadius: 10}} />
                  :
                  <Thumbnail source={require('../img/img-default.jpg')} square large style={{borderRadius: 10}} />
                }
              </Left>
              <Body>
                <View style={{paddingRight: 10}}>
                  <HTMLView 
                    value={res.title.rendered} 
                    stylesheet={html}
                    textComponentProps={{ style: html.titleStyle }}
                  />
                </View>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Icon type="AntDesign" name="clockcircleo" style={{fontSize: 10, marginRight: 5, alignSelf: 'center'}}/>
                  <Text note style={{textTransform: 'capitalize', fontSize: 12}}>
                    {moment(res.date).fromNow()}
                  </Text>
                </View>
              </Body>
            </ListItem>
          )) : <Empty text="Data Tidak Ditemukan"/>}
        </List> 
      </View>
    )
  }
}
