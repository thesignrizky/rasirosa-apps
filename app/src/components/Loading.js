import React, { Component } from 'react';
import{ View, ActivityIndicator } from 'react-native';
import { global } from '../styles'
import { Text } from 'native-base';

export default class Loading extends Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View style={[global.centered]}>
          <ActivityIndicator
            style={[{ marginBottom: 10 }]}
            size="large"
            color="#00A2EA"
          />
          <Text style={{color: '#00A2EA'}}>Loading</Text>
        </View>
      </View>
    );
  }
}
