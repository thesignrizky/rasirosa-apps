import React, { Component } from 'react'
import{ View, Platform, Image } from 'react-native'
import { Container, Content, Text, Spinner, Icon, Drawer, H3 } from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { global } from '../styles'
import Constants from 'expo-constants'

export default class NewContainer extends Component {
  render() {
    const { loading } = this.props;
    return (
      <Container style={[{backgroundColor: '#fff'}]}>
        {loading && 
          <View style={[global.centered, {backgroundColor: 'rgba(0,0,0,0.6)', position:'absolute', top: 0, right: 0, bottom: 0, left: 0, zIndex: 2}]}>
            <View style={[{alignContent: 'center', backgroundColor: '#fff', padding: 20, borderRadius: 20, width: 120, height: 120}]}>
              <Spinner color="#00A2EA" style={{alignSelf: 'center', width: 60, height: 60}}/>
              <Text bold style={{color: '#00A2EA', textAlign: 'center'}}>Loading</Text>
            </View>
          </View>
        }
        <View style={{flex: 1, position: 'relative', zIndex: 1}}>
          {this.props.children}
        </View>
      </Container>
    );
  }
}
