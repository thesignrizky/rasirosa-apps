import Loading from './Loading';
import NewContainer from './NewContainer';
import Empty from './Empty';
import ListPost from './ListPost';

export{
    Loading,
    NewContainer,
    Empty,
    ListPost
}