import React, { Component } from 'react'
import { View, Image, RefreshControl } from 'react-native'
import { Content, Button, Text } from 'native-base'
import { Loading, NewContainer, ListPost } from '../../components'
import { global } from '../../styles'
import { setListPost } from '../../actions/postAction'

export default class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _loading: true,
      _loadingFloat: false,
      _post: [],
      _per_page: 10,
      _totalPost: 0,
    }
  }

  componentDidMount() {
    return this.getData();
  }

  componentWillUnmount(){
    this.setState({})
  }
  
  async getData(){
    const { params: { data } } = this.props.route;
    const { navigation } = this.props;
    const payload = { 
      per_page: this.state._per_page,
      page: 1,
      filter: {
        category_name: data.slug
      }
    }
    var fetch = await setListPost(payload);
    navigation.setOptions({ title: data.title });
    return this.setState({
      _loading: false,
      _loadingFloat: false,
      _post: fetch.data.length > 0 ? fetch.data : [],
      _totalPost: fetch.data.length > 0 ? fetch.data[0].totalPost : 0
    })
  }

  onRefresh(){
    return this.setState({
      _loading: true,
      _post: [],
      _per_page: 10,
    }, () => this.getData())
  }

  setCurrentReadOffset = (event) => {
    let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    const { _totalPost, _per_page } = this.state;
    if(_per_page <= _totalPost ){
      if(scrollHeight == event.nativeEvent.contentSize.height){
        return this.setState({ _per_page: this.state._per_page + 10, _loadingFloat: true }, () => {
          return this.getData()
        })      
      }
    }
  }
  
  render() {
    const {_loading, _post, _loadingFloat} = this.state;
    const { navigation } = this.props;
    if(_loading){
      return <Loading />
    }
    return (
      <NewContainer loading={_loadingFloat}>
        <Content 
          refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />} 
          // scrollEventThrottle={100}
          onScroll={this.setCurrentReadOffset}
        >
          <Image 
            source={_post.length > 0 && _post[0].category_thumbnail ? {uri: _post[0].category_thumbnail} : require('../../img/img-default.jpg')}
            style={{width: null, height: 150}}
          />
          <ListPost data={_post} passProps={this.props} />
        </Content>

        <View style={[global.container]}>
          <Button full rounded onPress={() => navigation.push('Related', { data: _post[0].related })}>
            <Text>Info Lainnya</Text>
          </Button>
        </View>
      </NewContainer>
    )
  }
}
