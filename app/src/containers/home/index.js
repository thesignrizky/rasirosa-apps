import React, { Component } from 'react'
import { RefreshControl } from 'react-native'
import { Content } from 'native-base'
import { Loading, NewContainer } from '../../components'
import { setHomePost } from '../../actions/postAction'
import { Slider, SliderAds, ListPost, Category, Menu } from './partial'

export default class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _loading: true,
       _post: [],
       _menu: []
    }
  }

  componentDidMount() {
    return this.getData();
  }
  
  async getData(){
    var data = await setHomePost();
    return this.setState({
      _loading: false,
      _post: data.post,
      _menu: data.menu
    })
  }

  onRefresh(){
    return this.setState({
      _loading: true,
      _post: []
    }, () => this.getData())
  }
  
  render() {
    const {_loading, _menu, _post: { slider, sliderAds, newPost, beritaPilihan, newVideo, videoPilihan }} = this.state;
    if(_loading){
      return <Loading />
    }
    return (
      <NewContainer header={_menu} passProps={this.props}>
        <Menu menu={_menu} passProps={this.props}>
          <Content refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />} >
            <Slider data={slider}/>
            <Category data={slider} passProps={this.props}/>
            <SliderAds data={sliderAds}/>
            <ListPost title="Berita terbaru" data={newPost} passProps={this.props} />
            <ListPost title="Berita Pilihan" data={beritaPilihan} passProps={this.props} />
            <ListPost title="Video Terbaru" data={newVideo} passProps={this.props} />
            <ListPost title="Video Pilihan" data={videoPilihan} passProps={this.props} />
          </Content>
        </Menu>
      </NewContainer>
    )
  }
}
