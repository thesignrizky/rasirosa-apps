import React, { Component } from 'react'
import { Dimensions, View } from 'react-native'
import { Text, Icon } from 'native-base'
import { global } from '../../../styles';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default class Category extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _content: [
          { title: 'Keamanan', slug: 'keamanan', icon: { name: 'security', type: 'MaterialIcons' } },
          { title: 'Keselamatan', slug: 'keselamatan', icon: { name: 'car-crash', type: 'FontAwesome5' } },
          { title: 'Hukum', slug: 'hukum', icon: { name: 'book-open', type: 'SimpleLineIcons' } },
          { title: 'Administrasi', slug: 'administrasi', icon: { name: 'notebook', type: 'SimpleLineIcons' } },
          { title: 'Informasi', slug: 'informasi', icon: { name: 'sound', type: 'Entypo' } },
          { title: 'Kemanusiaan', slug: 'kemanusiaan', icon: { name: 'addusergroup', type: 'AntDesign' } },
       ]
    }
  }
  
  render() {
    const { navigation } = this.props.passProps;
    const { _content } = this.state;
    const { width } = Dimensions.get('screen');
    return (
      <View style={[global.containers, {marginTop: 20}]}>
        <View style={[global.justify]}>
          {_content.map((res, i) => (
            <View key={i} style={{marginBottom: 20,  width: width / 4, height: width / 4}}>
              <TouchableOpacity onPress={() => navigation.push('Category', { data: res })}>
                <View style={{alignSelf: 'center', justifyContent: 'center', width: width / 5, height: width / 5, backgroundColor: '#00A2EA', borderRadius: 100, marginBottom: 5}}>
                  <Icon name={res.icon.name} type={res.icon.type} style={{textAlign: 'center', color: '#fff', fontSize: 40}} />
                </View>
                <Text bold style={{textAlign: 'center'}}>{res.title}</Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
      </View>
    )
  }
}
