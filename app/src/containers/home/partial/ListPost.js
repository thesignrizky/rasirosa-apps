import React, { Component } from 'react'
import { View } from 'react-native'
import { List, ListItem, Thumbnail, Text, Left, Body, H3, Icon } from 'native-base'
import { global } from '../../../styles';
import moment from 'moment';
import { Empty } from '../../../components';

export default class ListPost extends Component {
  render() {
    const { data, title } = this.props;
    const { navigation } = this.props.passProps;
    return (
      <View style={{marginVertical: 20}}>
        {title &&
          <View style={[global.l_container, {flexDirection: 'row',  marginBottom: 10, height: 25}]}>
            <View style={{width: 5, backgroundColor: '#FD1713', marginRight: 10}}/>
            <H3 bold style={[{fontSize: 22,  alignSelf: 'center'}]}>
              {title}
            </H3>
          </View>
        }
        <List style={{backgroundColor: '#fff'}}>
          {data.length > 0 ? data.map((res, i) => (
            <ListItem thumbnail key={i} button onPress={() => navigation.push('Post', {data: res})}>
              <Left>
                {res.thumbnail.type === 'photo' ?
                  <Thumbnail source={res.thumbnail.file ? {uri: res.thumbnail.file} : require('../../../img/img-default.jpg')} square large style={{borderRadius: 10}} />
                  :
                  <Thumbnail source={require('../../../img/img-default.jpg')} square large style={{borderRadius: 10}} />
                }
              </Left>
              <Body>
                <H3 style={{fontSize: 14, lineHeight: 18, paddingRight: 10}}>{res.post_title}</H3>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Icon type="AntDesign" name="clockcircleo" style={{fontSize: 10, marginRight: 5, alignSelf: 'center'}}/>
                  <Text note style={{textTransform: 'capitalize', fontSize: 12}}>
                    {moment(res.post_date).fromNow()}
                  </Text>
                </View>
              </Body>
            </ListItem>
          )) : <Empty text="Data Tidak Ditemukan"/>}
        </List> 
      </View>
    )
  }

}
