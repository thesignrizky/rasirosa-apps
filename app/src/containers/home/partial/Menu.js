import React, { Component } from 'react'
import{ View, Platform, Image } from 'react-native'
import {  Content, Text, Icon, Drawer, H3, List, ListItem, Left, Right } from 'native-base'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { global } from '../../../styles'
import Constants from 'expo-constants'
import * as WebBrowser from 'expo-web-browser'


export default class Menu extends Component {
  render() {
    const { navigation } = this.props.passProps;
    return (
      <View style={{ flex: 1, marginTop: Platform.OS === 'android' ? Constants.statusBarHeight : Constants.statusBarHeight}}>        
        <Drawer
          type="overlay"
          // type="static"
          ref={(ref) => this._drawer = ref}
          tapToClose={true}
          tweenDuration={100}
          content={this.renderDrawer()}
          openDrawerOffset={100}
          side="right"
          negotiatePan={true}
        >
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <View style={[global.justify, { backgroundColor: '#fff', borderBottomColor: '#ddd', borderBottomWidth: 1}]}>
              <View>
                <Image
                  style={{ width: 50, height: 50, marginLeft: 20}}
                  source={require('../../../img/logo.png')}
                />
              </View>
              <View style={{alignSelf: 'center', justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
                <TouchableOpacity onPress={() => navigation.push('Search')}>
                  <View style={{padding: 10, alignSelf: 'center'}}>
                    <Icon name="search1" type="AntDesign" style={{fontSize: 22}}/>
                  </View>
                </TouchableOpacity>
                
                <TouchableOpacity onPress={() => this._drawer._root.open()}>
                  <View style={{padding: 10, paddingRight: 20, alignSelf: 'center'}}>
                    <Icon name="menu" type="Feather" />
                  </View>
                </TouchableOpacity>

              </View>
            </View>
            {this.props.children}
          </View>
        </Drawer>
      </View>
    );
  }

  renderDrawer(){
    const { menu } = this.props;
    const { navigation } = this.props.passProps;
    return(
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        <H3 style={{backgroundColor: '#00A2EA', padding: 20, color: '#fff'}}>
          Menu
        </H3>
        <Content>
          <List>
            <ListItem button onPress={() => navigation.push('Page', { slug: 'tentang-kami' })}>
              <Left><Text bold>Tentang Kami</Text></Left>
              <Right><Icon name="arrow-forward" /></Right>
            </ListItem>
            {menu && Object.values(menu).map((res, i) => {
              return(
                <View key={i}>
                  <ListItem button onPress={() => navigation.push('Category', { data: res })}>
                    <Left><Text bold>{res.title}</Text></Left>
                    <Right><Icon name="arrow-forward" /></Right>
                  </ListItem>
                  {res.children.map((_res, _i) => (
                    <ListItem key={_i} button onPress={() => navigation.push('Category', { data: _res })}>
                      <Left style={{ paddingLeft: 10}}>
                        <Text note>{_res.title}</Text>
                      </Left>
                      <Right><Icon name="arrow-forward" /></Right>
                    </ListItem>
                  ))}
                </View>
              )
            })}
            <ListItem button onPress={() => this._handlePressButtonAsync()}>
              <Left><Text bold>Monev</Text></Left>
              <Right><Icon name="arrow-forward" /></Right>
            </ListItem>
          </List>
        </Content>
      </View>
    )
  }

  _handlePressButtonAsync = async () => {
    await WebBrowser.openBrowserAsync('https://monev.rasirosakorlantas.id');
  };

}
