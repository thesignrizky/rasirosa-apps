import React, { Component } from 'react'
import { Image, View } from 'react-native'
import Swiper from 'react-native-swiper'

export default class Slider extends Component {
  render() {
    const { data } = this.props;
    const exp = /(<img\s+[^>]*src="([^"]*)"[^>]*>)/gm;
    return (
      <View style={{flex: 1, height: 200}}>
        <Swiper 
          autoplay={true}
          loop={true}
          showsButtons={false} 
          activeDotStyle={global.activeDotStyle} 
          dotStyle={global.dotStyle} 
          paginationStyle={{ bottom: 10 }}
        >
          {data && data.match(exp).map((res, i) => {
            const expSrc = /(https:\/\/([^"]*))/gm;
            const img = res.match(expSrc);
            return(
              <Image source={{uri: img[0]}} style={{width: null, height: 200}} key={i} />
            )
          })}
        </Swiper>
      </View>
    )
  }
}
