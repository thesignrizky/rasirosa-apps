import React, { Component } from 'react'
import { Image, View } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';
import Swiper from 'react-native-swiper'
import { global } from '../../../styles'
import * as WebBrowser from 'expo-web-browser'

export default class SliderAds extends Component {
  render() {
    const { data } = this.props;
    const exp = /(<div id="[^"]*?[^"]*?" class="rggclGridImgCntr">.*?<\/div>)/gm;
    return (
      <View style={[global.container, {flex: 1, height: 100}]}>
        <Swiper 
          autoplay={true}
          loop={true}
          showsButtons={false} 
          activeDotStyle={global.activeDotStyle} 
          dotStyle={global.dotStyle} 
          showsPagination={false}
          paginationStyle={{ bottom: 10 }}
        >
          {data && data.match(exp).map((res, i) => {
            const expSrc = /(http.:\/\/([^"]*))/gm;
            const img = res.match(expSrc);
            return(
              <TouchableOpacity key={i} onPress={() => this._handlePressButtonAsync(img[0])}>
                <Image source={{uri: img[1]}} style={{width: null, height: 100}} />
              </TouchableOpacity>
            )
          })}
        </Swiper>
      </View>
    )
  }


  _handlePressButtonAsync = async (url) => {
    await WebBrowser.openBrowserAsync(url);
  };

}
