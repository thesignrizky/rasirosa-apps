import Slider from './Slider';
import SliderAds from './SliderAds';
import ListPost from './ListPost';
import Category from './Category';
import Menu from './Menu';

export{
  Slider,
  SliderAds,
  ListPost,
  Category,
  Menu
}