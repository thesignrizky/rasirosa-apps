import Home from './home';
import Post from './post';
import Category from './category';
import Page from './page';
import Related from './related';
import Search from './search';

export{
    Home,
    Post,
    Category,
    Page,
    Related,
    Search
}