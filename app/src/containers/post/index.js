import React, { Component } from 'react'
import { View, Image, Dimensions, RefreshControl } from 'react-native'
import { Content, Text, H3, Icon } from 'native-base';
import { Empty, Loading, NewContainer } from '../../components'
import { global, html } from '../../styles'
import { setDetailPost } from '../../actions/postAction'
import HTMLView from 'react-native-htmlview';
import moment from 'moment';
import { ListPost } from '../home/partial';
import YoutubePlayer from "react-native-youtube-iframe";
import * as Linking from 'expo-linking';
const { width } = Dimensions.get('window');

export default class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _loading: true,
       _post: null
    }
  }

  componentDidMount() { 
    return this.getData();
  }

  componentWillUnmount(){
    this.setState({})
  }
  
  async getData(){
    const { params: { data } } = this.props.route;
    var fetch = await setDetailPost(data.post_name);
    return this.setState({
      _loading: false,
      _post: fetch.data ? fetch.data : null
    })
  }

  onRefresh(){
    return this.setState({
      _loading: true,
      _post: []
    }, () => this.getData())
  }
  
  render() {
    const {_loading, _post} = this.state;
    if(_loading){
      return <Loading />
    }
    return (
      <NewContainer>
        <Content refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />}>
          {_post ? 
              <>
              <View style={[global.container, {marginTop: 10, marginBottom: 30}]}>
                <View style={{marginBottom: 20}}>
                  <HTMLView 
                    value={_post.title.rendered} 
                    stylesheet={html}
                    renderNode={this.renderNodeTitle}
                  />
                  <View style={{flexDirection: 'row', marginTop: 5}}>
                    <Icon type="AntDesign" name="clockcircleo" style={{fontSize: 10, marginRight: 5, alignSelf: 'center'}}/>
                    <Text note style={{fontSize: 12}}>
                      {moment(_post.date).fromNow()}
                    </Text>
                  </View>
                </View>
                <HTMLView 
                  stylesheet={html}
                  value={_post.content.rendered.replace(/(\r\n|\n|\r|<\s*figure.*?>|<\/figure>)/gm, '')} 
                  renderNode={this.renderNode}
                  textComponentProps={{ style: html.defaultStyle }}
                />

              </View>
              <ListPost title="Info Lainnya" data={_post.related} passProps={this.props} />
            </>
            :
            <Empty text="Data Tidak Ditemukan"/>
          }
          
        </Content>
      </NewContainer>
    )
  }


  renderNodeTitle(node, index) {
    return(
      <View key={index}>
        <H3>{node.data}</H3>
      </View>
    )
  }

  renderNode(node, index) {
    if (node.name === 'img') {
      const attr = node.attribs;
      const ratio = attr.width / attr.height;
      attr.width = width - 40;
      attr.height = (attr.width  / ratio) - 20;
      return (
        <Image
          key={index}
          style={{ width: Number(attr.width), height: Number(attr.height), marginBottom: 20}}
          source={{ uri: attr.src }} 
        />
      );
    }else if(node.name === 'iframe'){
      const a = node.attribs;
      const url = Linking.parse(a.src)
      const id = url.path.split('/')[1]
      return (
        <View key={index} style={{width: Number(width-40), height: Number(200)}}>
          <YoutubePlayer
            height={200}
            play={false}
            videoId={id}
            onChangeState={(state) => console.log('onChangeState', state) }
          />
        </View>
      );
    }
  }

}
