import React, { Component } from 'react'
import { Content, Container } from 'native-base'
import { ListPost } from '../home/partial';

export default class index extends Component {
  render() {
    const { params: {data} } = this.props.route;
    return (
      <Container>
        <Content style={{marginBottom: 20}}>
          <ListPost data={data} passProps={this.props} />
        </Content>
      </Container>
    )
  }
}