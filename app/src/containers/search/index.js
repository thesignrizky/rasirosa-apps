import React, { Component } from 'react'
import { Platform } from 'react-native'
import { Content, Icon, Header, Item, Input} from 'native-base';
import { NewContainer, ListPost } from '../../components'
import { setListPost } from '../../actions/postAction'
import Constants from 'expo-constants'

export default class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _loading: true,
       _post: [],
       _search: '',
       _per_page: 10,
       _totalPost: 0,
    }
  }
  
  async componentDidMount(){
    return this.getData()
  }

  async getData(){
    var payload = {
      per_page: this.state._per_page,
      page: 1,
      search: this.state._search
    }
    var fetch = await setListPost(payload);
    return this.setState({
      _loading: false,
      _post: fetch.data.length > 0 ? fetch.data : [],
      _totalPost: fetch.data.length > 0 ? fetch.data[0].totalPost : 0
    })
  }

  onSearch(){
    return this.setState({
      _per_page: 10,
      _loading: true,
    }, () => this.getData())
  }

  setCurrentReadOffset = (event) => {
    let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    const { _totalPost, _per_page } = this.state;
    if(_per_page <= _totalPost ){
      if(scrollHeight == event.nativeEvent.contentSize.height){
        return this.setState({ _per_page: this.state._per_page + 10, _loading: true }, () => {
          return this.getData()
        })      
      }
    }
  }

  render() {
    const {_loading, _post, _search } = this.state;
    return (
      <>
        <Header searchBar rounded style={{marginTop: Platform.OS === 'android' ? Constants.statusBarHeight : 0}}>
          <Item>
            <Icon name="ios-search" />
            <Input 
              placeholder="Cari Berita" 
              onChangeText={(e) => this.setState({_search: e}) }
              onSubmitEditing={() => this.onSearch() }
            />
          </Item>
        </Header>
        <NewContainer loading={_loading}>
          <Content
            // scrollEventThrottle={300}
            onScroll={this.setCurrentReadOffset}
          >
            <ListPost title={`Cari: ${_search}`} data={_post} passProps={this.props} />
          </Content>
        </NewContainer>
      </>
    )
  }

  componentWillUnmount(){
    this.setState({})
  }

}
