// @flow

import variable from './../variables/platform';

export default (variables /* : * */ = variable) => {
  const textTheme = {
    fontSize: variables.DefaultFontSize,
    fontFamily: variables.fontFamily,
    color: variables.textColor,
    '.note': {
      color: '#707070',
      fontSize: variables.noteFontSize
    },
    '.bold': {
      fontFamily: variables.titleFontfamily,
      fontSize: variables.noteFontSize
    },
    '.title': {
      fontFamily: variables.titleFontfamily,
      // fontSize: variables.noteFontSize
    },
    '.danger': {
      color: 'red',
    },
    '.success': {
      color: 'green',
    },

  };

  return textTheme;
};
